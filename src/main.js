// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'vue2-admin-lte/src/lib/css'
import 'vue2-admin-lte/src/lib/script'
import 'font-awesome/css/font-awesome.css'
import 'bootstrap-sass/assets/stylesheets/_bootstrap-sprockets.scss'
import VueFormGenerator from 'vue-form-generator'
import Multiselect from 'vue-multiselect'

window.$ = window.jQuery = require('jquery')

Vue.config.productionTip = false
Vue.use(VueFormGenerator)
Vue.component('multiselect', Multiselect)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
