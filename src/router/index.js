import Vue from 'vue'
import Router from 'vue-router'
import CadastroSabor from '../components/CasdatroSabor.vue'
import CadastroIngred from '../components/CadastroIngred.vue'
import VendaPizza from '../components/VendaPizza.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/novo/sabor',
      name: 'novo-sabor',
      component: CadastroSabor
    },
    {
      path: '/novo/ingred',
      name: 'novo-ingred',
      component: CadastroIngred
    },
    {
      path: '/venda/pizza',
      name: 'venda-pizza',
      component: VendaPizza
    }
  ]
})
