//function load (component) {
//  return require(`components/${component}.vue`)
//}
export default [
  {
    icon: 'fa fa-plus',
    label: 'Cadastro',
    key: 1,
    subMenu: [
      {
        icon: 'fa fa-plus fa-0.1x',
        label: 'Novo sabor',
        link: 'novo-sabor'
      },
      {
        icon: 'fa fa-plus fa-0.1x',
        label: 'Novo Ingrediente',
        link: 'novo-ingred'
      }
    ]
  },
  {
    icon: 'fa fa-cart-plus',
    label: 'Movimentação do dia',
    key: 1,
    subMenu: [
      {
        icon: 'fa fa-plus fa-0.1x',
        label: 'Adicionar pizzas vendidas',
        link: 'venda-pizza'
      }
    ]
  }
]

